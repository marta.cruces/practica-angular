import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { PostDTO } from '../home-dto';
import { HomeService } from '../home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  publicPosts$: Observable<PostDTO[]>;

  constructor(private homeService: HomeService, private router: Router) { }

  ngOnInit(): void {
    this.publicPosts$ = this.homeService.getPosts();
    console.log(this.publicPosts$);
  }

  onSelect(postID){
    this.router.navigate(['home', postID]);
  }
}
