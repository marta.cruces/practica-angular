export interface Comment {
    nickname: string;
    content: string;
}
