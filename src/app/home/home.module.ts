import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppHomeComponent } from './app-home/app-home.component';
import { DetailPostComponent } from './detail-post/detail-post.component';
import { HomeComponent } from './home/home.component';

const ROUTES: Routes = [
  {path: '', component: AppHomeComponent,
    children: [
      {path: 'home', component: HomeComponent},
      {path: 'home/:id', component: DetailPostComponent}
    ]}
];

@NgModule({
  declarations: [HomeComponent, AppHomeComponent, DetailPostComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule.forChild(ROUTES)
  ],
  exports: [
    HomeComponent,
    AppHomeComponent,
    DetailPostComponent
  ]
})
export class HomeModule { }
