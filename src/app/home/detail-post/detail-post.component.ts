import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { DetailProxyService } from '../detail-proxy.service';
import { PostDTO } from '../home-dto';

@Component({
  selector: 'app-detail-post',
  templateUrl: './detail-post.component.html',
  styleUrls: ['./detail-post.component.css']
})
export class DetailPostComponent implements OnInit {

  post: Observable<PostDTO>;
  id: string;

  constructor(private activatedRoute: ActivatedRoute, private proxyDetail: DetailProxyService) { }

  ngOnInit(): void {
    this.getPostById();
  }
  getPostById() {
    this.id = this.activatedRoute.snapshot.params.id;
    this.post = this.proxyDetail.getPostById(this.id);
  }
}
