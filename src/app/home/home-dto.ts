export interface PostDTO {
    _id: string;
    author: Author;
    title: string;
    text: string;
    comments: Comment;
}

interface Author {
    _id: string;
    username: string;
}

interface Comment {
    nickname: string;
    content: string;
}
